import java.util.*;

public class LongStack {

   public static void main (String[] argum) {
       LongStack m1 = new LongStack();
       m1.push (5);
       m1.push (4);
       m1.push(-15);
       m1.op("-");
       System.out.println(interpret("   \t \t356  \t \t"));
       String k = m1.toString();
       System.out.println(k);
   }

   private int control;
   private long[] rpn;

   //contructor
   LongStack(){
       this(10);
   }

   //contructor
   LongStack(int size) { //the size of new contructor is given
       rpn = new long[size];
       control = -1;
   }

   // clone stack
   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack magasin = new LongStack(rpn.length);
      magasin.control = control;
      if(!stEmpty()){
          for(int i=0; i<=control; i++){
              magasin.rpn[i] = rpn[i];
          }
      }
      return magasin;
   }

   // check if stack is empty
   public boolean stEmpty() {
       return (control == -1);
   }

   // insert new value and move top
   public void push (long a) {
       control += 1;
       rpn[control] = a;
   }

   // take the last entered value and move top
   public long pop() {
       if(stEmpty()){
           throw new IndexOutOfBoundsException("LongStack is empty");
       }
       long number = rpn[control];
       control -= 1;
       return number;
   } // pop

    // do mathematical operation and check if operator is invalid or stack is too small
   public void op (String s) {
       if(rpn.length < 2){
           throw new RuntimeException("RuntimeException");
       }
       long number1 = pop();
       long number2 = pop();
       switch (s) {
           case "+":
               push(number2 + number1);
               break;
           case "-":
               push(number2 - number1);
               break;
           case "/":
               push(number2 / number1);
               break;
           case "*":
               push(number2 * number1);
               break;
           default:
               throw new RuntimeException("Invalid operator");
       }
   }

   //return the current last number in stack
   public long tos() {
      return rpn[control];
   }

   //check if two stacks are equal
   @Override
   public boolean equals (Object o) {
       if(((LongStack)o).control != control){ return false; }
       if (((LongStack)o).stEmpty() && stEmpty()){ return true; }
       for(int i=0; i<=control; i++){
           if(rpn[i] != ((LongStack)o).rpn[i]){ return false; }
       }
       return true;
   }

   //convert LongStack into string
   @Override
   public String toString() {
       StringBuilder longStackString = new StringBuilder();
       if(stEmpty()){return " ";}
       for(int i = 0; i<=control; i++){
            longStackString.append(String.valueOf(rpn[i])).append(" ");
       }
     return longStackString.toString();
   }

   public static long interpret (String pol) {
       if(pol.length() != 0){
           ArrayList<String> list = new ArrayList<>(Arrays.asList(pol.trim().split("\\s+"))); // https://stackoverflow.com/questions/225337/how-do-i-split-a-string-with-any-whitespace-chars-as-delimiters
           LongStack stack = new LongStack();
           for (int i=0; i<list.size(); i++){
               //check if element is parsable, if not, do operation
               try {
                   long number = Long.parseLong(list.get(i));
                   stack.push(number);
               }catch (NumberFormatException e){
                   stack.op(list.get(i));
               }

           }
           long result = stack.pop();
           // check if there is no numbers left in stack
           if(stack.control != -1){
               throw new RuntimeException("Redundant numbers in expression");
           }
           return result;
       }
       throw new RuntimeException("Empty string");
   }
}

